package bot;

import bot.controller.Controller;
import bot.model.Model;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private Model model = new Model();
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("IziDungeon.fxml"));
        FXMLLoader loader1 = new FXMLLoader(getClass().getClassLoader().getResource("IziDungeon.fxml"));
        Parent root = loader.load();
        Controller controller = loader.getController();
        controller.setModel(model);
        primaryStage.setTitle("IziDungeon");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
