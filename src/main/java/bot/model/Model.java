package bot.model;

import bot.jsonsave.PlayerListRegistred;
import bot.jsonsave.Salvataggio;
import bot.pg.Boss;
import bot.pg.Player;
import bot.twitchbot.Bot;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Model implements BotTrigger {

    private Boss boss;
    private Player player;
    private Party party;
    private PlayerListRegistred playerListRegistred;
    private Salvataggio salvataggio;
    private String nomeBot = "IziDungeonBot", _OauthKey, nomeCanale;
    private ArrayList<Class> comandi;
    private Bot bot;
    private ArrayList<Player> listaPlayerRegistrati, listaPlayerJoinati;
    private ArrayList<Boss> listaBossRun;
    private ArrayList<String> listaNomiBoss;
    private ArrayList<Integer> piattoScommesse;
    private int qntChest, valoreChest, durataJoin, diamantiNP, attaccoNP, difesaNP;
    private boolean[] radiocheckDifficolta = {true, false, false, false};

    public Model() {
        comandi = new ArrayList<Class>();

        //TODO RECOVER LAST USAGE FROM DEFAULT JSON'S LOCATION

        listaPlayerRegistrati = new ArrayList<Player>();
        listaPlayerJoinati = new ArrayList<Player>();
        piattoScommesse = new ArrayList<Integer>();
        listaBossRun = new ArrayList<Boss>();
        listaNomiBoss = new ArrayList<String>();
        if (new File(System.getProperty("user.home") + "/settingsGeneralIziD.json").exists()) {
            initJsonAttribute();
        } else {
            generateGeneralSetting();
        }
        generaListaBozz();
    }

    private HashMap<String, Integer> loadAttributeJson() {
        try {
            Gson gson = new Gson();
            File loadGeneral = new File(System.getProperty("user.home") + "/settingsGeneralIziD.json");
            FileReader fr = new FileReader(loadGeneral);
            BufferedReader br = new BufferedReader(fr);
            String json = br.readLine();
            HashMap<String, Integer> hm = gson.fromJson(json, new TypeToken<HashMap<String, Integer>>() {}.getType());
            return hm;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private HashMap<String, Integer> generalSettings;
    private void generateGeneralSetting() {
        File saveGeneral = new File(System.getProperty("user.home") + "/settingsGeneralIziD.json");
        try {
            Gson gson = new Gson();
            generalSettings = new HashMap<String, Integer>();
            generalSettings.put("QNTCHEST", 5);
            generalSettings.put("VCHEST", 1);
            generalSettings.put("DJOIN", 240);
            generalSettings.put("DIAMNP", 1000);
            generalSettings.put("ATKNP", 10);
            generalSettings.put("DEFNP", 5);
            generalSettings.put("DIFF", 0);
            Salvataggio save = new Salvataggio(generalSettings);
            String json = gson.toJson(save.getGeneralSettings(), new TypeToken<HashMap<String,Integer>>() {}.getType());
            saveGeneral.createNewFile();
            FileWriter fw = new FileWriter(saveGeneral);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(json);
            bw.flush();
            bw.close();
            initJsonAttribute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void replaceGeneralSettings() {
        new File(System.getProperty("user.home") + "/settingsGeneralIziD.json").delete();
            File saveGeneral = new File(System.getProperty("user.home") + "/settingsGeneralIziD.json");
            try {
                Gson gson = new Gson();
                generalSettings = new HashMap<String, Integer>();
                generalSettings.put("QNTCHEST", qntChest);
                generalSettings.put("VCHEST", valoreChest);
                generalSettings.put("DJOIN", durataJoin);
                generalSettings.put("DIAMNP", diamantiNP);
                generalSettings.put("ATKNP", attaccoNP);
                generalSettings.put("DEFNP", difesaNP);
                generalSettings.put("DIFF", getDifficoltaInt());
                Salvataggio save = new Salvataggio(generalSettings);
                String json = gson.toJson(save.getGeneralSettings(), new TypeToken<HashMap<String, Integer>>() {
                }.getType());
                saveGeneral.createNewFile();
                FileWriter fw = new FileWriter(saveGeneral);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(json);
                bw.flush();
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    private void initJsonAttribute() {
        generalSettings = loadAttributeJson();
            // GENERAL DEFAULT
        qntChest = generalSettings.get("QNTCHEST");
        valoreChest = generalSettings.get("VCHEST");
        durataJoin = generalSettings.get("DJOIN");
        diamantiNP = generalSettings.get("DIAMNP");
        attaccoNP = generalSettings.get("ATKNP");
        difesaNP = generalSettings.get("DEFNP");
            // TODO IMPOSTA DIFFICOLTA
    }

    public HashMap<String, Integer> getGeneralSettings() {
        return generalSettings;
    }

    private final int NUMERO_BOSS = 5;
    private ArrayList<Boss> generaListaBozz() {
        //DEBUG
        listaNomiBoss.add("Gerry Sgnotti");
        listaNomiBoss.add("2");
        listaNomiBoss.add("3");
        listaNomiBoss.add("4");
        listaNomiBoss.add("5");

        for (int i = 0; i < NUMERO_BOSS; i++) {
            listaBossRun.add(new Boss(listaNomiBoss.get(i), "Spettro", "image",
                    getRandomLv("Facile"), (10 * getRandomLv("Facile") /2),
                    ((10 * getRandomLv("Facile")) /2), i, ((10 * getRandomLv("Facile")) /2)));
        }
        return listaBossRun;
    }

    public ArrayList<Boss> getListaBossRun() {
        return listaBossRun;
    }

    public void setListaBossRun(ArrayList<Boss> listaBossRun) {
        this.listaBossRun = listaBossRun;
    }

    private BossTrigger bossTrigger;
    public void setBossTrigger(BossTrigger bossTrigger) {
        this.bossTrigger = bossTrigger;
    }

    private int getRandomLv(String difficolta) {
        Random rnd = new Random();
        int livello = 0;
        if (getDifficolta().equals("Facile")) {
            livello = rnd.nextInt((10 - 1) + 1) + 1;
        }
        if (getDifficolta().equals("Medio")) {
            livello = rnd.nextInt((20 - 10) + 10) + 10;
        }
        if (getDifficolta().equals("Difficile")) {
            livello = rnd.nextInt((40 - 20) + 20) + 20;
        }
        if (getDifficolta().equals("Ingiusto")) {
            livello = rnd.nextInt((99 - 40) + 40) + 40;
        }
        return livello;
    }

    public Player getFromListaRegistrati(String user) {
        for (int i = 0; i < listaPlayerRegistrati.size(); i++) {
            if (listaPlayerRegistrati.get(i).getIdTwitch().equals(user)) {
                return listaPlayerRegistrati.get(i);
            }
        }
        return null;
    }

    public void setComandiPersonalizzati(ArrayList<Class> comandi) {
        this.comandi = comandi;
        bot.setListaComandiPersonalizzati(comandi);
        bot.registerCommands(botTriggerUI, comandi);
        //TODO SALVARE NEL FILE JSON
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------
    private BotTrigger getM() {
        return this;
    }
    private BotTriggerUI botTriggerUI;
    public void setBotTriggerUI(BotTriggerUI botTriggerUI) {
        this.botTriggerUI = botTriggerUI;
    }

    private Model getModel() {
        return this;
    }
    private boolean isBotRun = false;
    private Thread botThread, tBB;
    public void connectBot() {
        //bossTrigger.initBoss(0);
        isBotRun = true;
        botThread = new Thread(new Runnable() {
            public void run() {
                synchronized (this) {
                    bot = new Bot(_OauthKey, comandi, nomeCanale, getModel());
                    bot.registerFeature();
                    bot.setBotTrigger(getM());
                    bot.joinChannel();
                    bot.registerCommands(botTriggerUI, comandi);
                    bot.sendMessage("/me " + nomeBot + " è arrivato per mettervi contro il male più assoluto che c'è!");
                    tBB.start();
                    notifyAll();
                    try {
                        wait(5000);
                        // INIZIA IL THREAD DEL COUNTDOWN PER IL JOIN
                        Runnable rJoin = bossTrigger.startJoin();
                        new Thread(rJoin).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        botThread.start();
        Runnable rBotBrain = botBrain();
        tBB = new Thread(rBotBrain);
    }
    // ---------------------------------------------------------------------------------------------------------------------------------------------
    public boolean isBotRun() {
        return isBotRun;
    }

    public void saveBotConfiguration(/*ConfigurationBot configurationBot*/) {
        synchronized (this) {
            try {
                Gson gson = new Gson();
                File config = new File(System.getProperty("user.home") + "/settingsIziD.json");
                Salvataggio save = new Salvataggio(bot.getConfigurationBot());
                HashMap<String, String> saveList = new HashMap<String, String>();
                saveList.put("ID", save.getConfigurationBot().getID());
                saveList.put("IDSECRET", save.getConfigurationBot().getSecretID());
                saveList.put("OAUTH", save.getConfigurationBot().getOauth());
                saveList.put("CHNAME", save.getConfigurationBot().getChannelName());
                String json = gson.toJson(saveList, new TypeToken<HashMap<String,String>>() {}.getType());
                config.createNewFile();
                FileWriter fileWriter = new FileWriter(config);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(json);
                bufferedWriter.flush();
                bufferedWriter.close();
                notifyAll();
                wait(1000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException ee) {
                ee.printStackTrace();
            }
        }
    }

    public void replaceTwitchConfiguration() {
        synchronized (this) {
            try {
                Gson gson = new Gson();
                File config = new File(System.getProperty("user.home") + "/settingsIziD.json");
             //   Salvataggio save = new Salvataggio(bot.getConfigurationBot());
                HashMap<String, String> saveList = new HashMap<String, String>();
                Bot botreplace = new Bot();
                saveList.put("ID", botreplace.getConfigurationBot().getID());
                saveList.put("IDSECRET", botreplace.getConfigurationBot().getSecretID());
                saveList.put("OAUTH", _OauthKey);
                saveList.put("CHNAME", nomeCanale);
                String json = gson.toJson(saveList, new TypeToken<HashMap<String,String>>() {}.getType());
                config.createNewFile();
                FileWriter fileWriter = new FileWriter(config);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(json);
                bufferedWriter.flush();
                bufferedWriter.close();
                notifyAll();
                wait(1000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException ee) {
                ee.printStackTrace();
            }
        }
    }
    public HashMap<String, String> loadInitialUiConfigurationTwitch() {
        HashMap<String, String> loadList = null;
        try {
            Gson gson = new Gson();
            File load = new File(System.getProperty("user.home") + "/settingsIziD.json");
            FileReader fileReader = new FileReader(load);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String json = bufferedReader.readLine();
            return loadList = gson.fromJson(json, new TypeToken<HashMap<String,String>>() {}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private boolean[] STATUS = {false, false, false}; // 0 STARTJOIN, 1 STARTSAVE, 2 STATOCOMBATTIMENTO
    private Thread botBrainThread;
    private Runnable botBrain() {
         botBrainThread = new Thread(new Runnable() {
            public void run() {
                synchronized (this) {
                    int indexBoss = 0;
                    boolean indexBossList[] = new boolean[listaBossRun.size()];
                    while (!isBotRun) {
                        if (bot.isRunning()) { isBotRun = true;}
                    }
                    while (isBotRun) {
                        notifyAll();
                        try {
                            if (STATUS[0]) {
                                bot.sendMessage("/me E' iniziato il countdown per il join: " + getDurataJoin() +
                                        " secondi");
                                party = new Party(); // INIZIALIZZO UN PARTY NUOVO AD OGNI INZIO JOIN
                                STATUS[0] = false;
                                setSTATUS(1, true);
                            }
                            if (STATUS[1]) {
                                saveBotConfiguration();
                                setSTATUS(1, false);
                            }
                            if (STATUS[2]) {
                                if (indexBossList[indexBoss]) {
                                    bot.sendMessage("/me E' iniziato il combattimento contro:" + boss.getNome());
                                    bossTrigger.initBoss(indexBoss);
                                }
                                setSTATUS(2, false);
                                //TODO INIZIA IL COMBATTIMENTO
                            }
                            wait (1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.err.println(bot);
                        }
                    }
                }
            }
        });
        return botBrainThread;
    }

    public boolean getSTATUS(int index) {
        return STATUS[index];
    }

    public void setSTATUS(int index, boolean newStatus) {
        this.STATUS[index] = newStatus;
    }

    private String getDifficolta() {
        if (radiocheckDifficolta[0]) {
            return "Facile";
        }
        if (radiocheckDifficolta[1]) {
            return "Medio";
        }
        if (radiocheckDifficolta[2]) {
            return "Difficile";
        }
        if (radiocheckDifficolta[3]) {
            return "Ingiusto";
        }
        return null;
    }

    private int getDifficoltaInt() {
        if (radiocheckDifficolta[0]) {
            return 0;
        }
        if (radiocheckDifficolta[1]) {
            return 1;
        }
        if (radiocheckDifficolta[2]) {
            return 2;
        }
        if (radiocheckDifficolta[3]) {
            return 3;
        }
        return -1;
    }

    public boolean[] getRadiocheckDifficolta() {
        return radiocheckDifficolta;
    }

    public void setRadiocheckDifficolta(boolean[] radiocheckDifficolta) {
        this.radiocheckDifficolta = radiocheckDifficolta;
    }

    public Party getParty() {
        return party;
    }

    public ArrayList<Player> getListaPlayerJoinati() {
        return listaPlayerJoinati;
    }

    public int getQntChest() {
        return qntChest;
    }

    public void setQntChest(int qntChest) {
        this.qntChest = qntChest;
    }

    public int getValoreChest() {
        return valoreChest;
    }

    public void setValoreChest(int valoreChest) {
        this.valoreChest = valoreChest;
    }

    public int getDurataJoin() {
        return durataJoin;
    }

    public void setDurataJoin(int durataJoin) {
        this.durataJoin = durataJoin;
    }

    public int getDiamantiNP() {
        return diamantiNP;
    }

    public void setDiamantiNP(int diamantiNP) {
        this.diamantiNP = diamantiNP;
    }

    public int getAttaccoNP() {
        return attaccoNP;
    }

    public void setAttaccoNP(int attaccoNP) {
        this.attaccoNP = attaccoNP;
    }

    public int getDifesaNP() {
        return difesaNP;
    }

    public void setDifesaNP(int difesaNP) {
        this.difesaNP = difesaNP;
    }

    public ArrayList<String> getBotChat() {
        return bot.getChat();
    }

    public String getNomeCanale() {
        return nomeCanale;
    }

    public void setNomeCanale(String nomeCanale) {
        this.nomeCanale = nomeCanale;
    }

    public Bot getBot() {
        return bot;
    }

    public void setBot(Bot bot) {
        this.bot = bot;
    }

    public String getNomeBot() {
        return nomeBot;
    }

    public void setNomeBot(String nomeBot) {
        this.nomeBot = nomeBot;
    }

    public String get_OauthKey() {
        return _OauthKey;
    }

    public void set_OauthKey(String _OauthKey) {
        this._OauthKey = _OauthKey;
    }

    public Boss getBoss() {
        return boss;
    }

    public void setBoss(Boss boss) {
        this.boss = boss;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<Player> getListaPlayerRegistrati() {
        return listaPlayerRegistrati;
    }

    public Salvataggio getSalvataggio() {
        return salvataggio;
    }

    public void setSalvataggio(Salvataggio salvataggio) {
        this.salvataggio = salvataggio;
    }
}

