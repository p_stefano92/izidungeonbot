package bot.model;

import bot.pg.Boss;

import java.util.ArrayList;

public interface BossTrigger {
    void initBoss(int index);
    Runnable startJoin();
}
