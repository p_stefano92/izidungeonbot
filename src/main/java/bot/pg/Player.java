package bot.pg;

public class Player {
    private String idTwitch, idWasdit;
    private int diamanti, livello, attacco, difesa, esperienza;

    public Player(String idTwitch, int diamantiPool, int livello, int attacco, int difesa) {
        this.idTwitch = idTwitch;
        this.diamanti = diamantiPool;
        this.livello = livello;
        this.attacco = attacco;
        this.difesa = difesa;
        this.esperienza = 0;
        this.idWasdit = "";
    }

    public String getIdWasdit() {
        return idWasdit;
    }

    public void setIdWasdit(String idWasdit) {
        idWasdit = idWasdit;
    }

    public String getIdTwitch() {
        return idTwitch;
    }

    public void setIdTwitch(String idTwitch) {
        this.idTwitch = idTwitch;
    }

    public int getDiamanti() {
        return diamanti;
    }

    public void setDiamanti(int diamanti) {
        this.diamanti = diamanti;
    }

    public int getLivello() {
        return livello;
    }

    public void setLivello(int livello) {
        this.livello = livello;
    }

    public int getAttacco() {
        return attacco;
    }

    public void setAttacco(int attacco) {
        this.attacco = attacco;
    }

    public int getDifesa() {
        return difesa;
    }

    public void setDifesa(int difesa) {
        this.difesa = difesa;
    }

    public int getEsperienza() {
        return esperienza;
    }

    public void setEsperienza(int esperienza) {
        this.esperienza = esperienza;
    }

}
