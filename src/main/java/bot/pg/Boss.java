package bot.pg;

public class Boss {
    private String nome, tipo, imageID;
    private int livello, difesa, attacco, difesaBuffata, attaccoBuffato, ID, HP;
    private boolean sconfitto = false;

    public Boss(String nome, String tipo, String imageID, int livello, int difesa, int attacco, int ID, int HP) {
        this.nome = nome;
        this.tipo = tipo;
        this.imageID = imageID;
        this.livello = livello;
        this.difesa = difesa;
        this.attacco = attacco;
        this.ID = ID;
        this.HP = HP;
    }

    public boolean isSconfitto() {
        return sconfitto;
    }

    public void setSconfitto(boolean sconfitto) {
        this.sconfitto = sconfitto;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getLivello() {
        return livello;
    }

    public void setLivello(int livello) {
        this.livello = livello;
    }

    public int getDifesa() {
        return difesa;
    }

    public void setDifesa(int difesa) {
        this.difesa = difesa;
    }

    public int getAttacco() {
        return attacco;
    }

    public void setAttacco(int attacco) {
        this.attacco = attacco;
    }

    public int getDifesaBuffata() {
        return difesaBuffata;
    }

    public void setDifesaBuffata(int difesaBuffata) {
        this.difesaBuffata = difesaBuffata;
    }

    public int getAttaccoBuffato() {
        return attaccoBuffato;
    }

    public void setAttaccoBuffato(int attaccoBuffato) {
        this.attaccoBuffato = attaccoBuffato;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }
}
