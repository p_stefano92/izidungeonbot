package bot.settings;

import java.util.ArrayList;

public class SettingsGeneral {
    private int qntChest;
    private int valoreChest;
    private int durataJoin;
    private int diamantiNP;
    private int attaccoNP;
    private int difesaNP;
    private ArrayList<SettingsGeneral> settingsGenerals;

    public SettingsGeneral(int qntChest, int valoreChest, int durataJoin, int diamantiNP, int attaccoNP, int difesaNP) {
        this.qntChest = qntChest;
        this.valoreChest = valoreChest;
        this.durataJoin = durataJoin;
        this.diamantiNP = diamantiNP;
        this.attaccoNP = attaccoNP;
        this.difesaNP = difesaNP;
        settingsGenerals = new ArrayList<SettingsGeneral>();
    }

    public void setSettingGenerals(int qntChest, int valoreChest, int durataJoin, int diamantiNP, int attaccoNP, int difesaNP) {
        settingsGenerals.add(new SettingsGeneral(qntChest, valoreChest, durataJoin, diamantiNP, attaccoNP, difesaNP));
    }

    public int getQntChest() {
        return qntChest;
    }

    public void setQntChest(int qntChest) {
        this.qntChest = qntChest;
    }

    public int getValoreChest() {
        return valoreChest;
    }

    public void setValoreChest(int valoreChest) {
        this.valoreChest = valoreChest;
    }

    public int getDurataJoin() {
        return durataJoin;
    }

    public void setDurataJoin(int durataJoin) {
        this.durataJoin = durataJoin;
    }

    public int getDiamantiNP() {
        return diamantiNP;
    }

    public void setDiamantiNP(int diamantiNP) {
        this.diamantiNP = diamantiNP;
    }

    public int getAttaccoNP() {
        return attaccoNP;
    }

    public void setAttaccoNP(int attaccoNP) {
        this.attaccoNP = attaccoNP;
    }
    public int getDifesaNP() {
        return difesaNP;
    }

    public void setDifesaNP(int difesaNP) {
        this.difesaNP = difesaNP;
    }
}
