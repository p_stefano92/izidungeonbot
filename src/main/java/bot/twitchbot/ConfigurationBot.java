package bot.twitchbot;

import me.philippheuer.twitch4j.TwitchClient;
import me.philippheuer.twitch4j.model.Channel;

public class ConfigurationBot {

    private Channel channel;
    private TwitchClient twitchClient;
    private String Oauth, ID, secretID, channelName;

    public ConfigurationBot(String ID, String secretID) {
        this.ID = ID;
        this.secretID = secretID;
    }

    public ConfigurationBot(TwitchClient twitchClient, String oauth, String ID, String secretID, String channelName) {
        this.twitchClient = twitchClient;
        this.Oauth = oauth;
        this.ID = ID;
        this.secretID = secretID;
        this.channelName = channelName;
    }

    public TwitchClient getTwitchClient() {
        return twitchClient;
    }

    public void setTwitchClient(TwitchClient twitchClient) {
        this.twitchClient = twitchClient;
    }

    public String getOauth() {
        return Oauth;
    }

    public void setOauth(String oauth) {
        Oauth = oauth;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSecretID() {
        return secretID;
    }

    public void setSecretID(String secretID) {
        this.secretID = secretID;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public void setChannel(String channelName) {
        this.channel = this.twitchClient.getChannelEndpoint().getChannel(channelName);
    }

    public Channel getChannel() {
        return channel;
    }

    public String getChannelName() {
        return channelName;
    }
}
