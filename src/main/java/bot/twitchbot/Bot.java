package bot.twitchbot;

import bot.model.BotTrigger;
import bot.model.BotTriggerUI;
import bot.model.Model;
import bot.twitchbot.commands.general.*;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import me.philippheuer.twitch4j.TwitchClient;
import me.philippheuer.twitch4j.TwitchClientBuilder;
import me.philippheuer.twitch4j.endpoints.ChannelEndpoint;
import me.philippheuer.twitch4j.endpoints.StreamEndpoint;
import me.philippheuer.twitch4j.model.Channel;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;


public class Bot {

    private String _OauthKey;
    protected final String IDAPP = "3riobbvhqdtahf9ajwkhsvgmcf6gk5",
            IDSECRET = "3tpnt2c0g5o38m3wpxybr377lad96x";
    private ArrayList<Class> listaComandi;
    private TwitchClient twitchClient;
    private ChannelEndpoint channelEndPoint;
    private Channel channel;
    private ArrayList<String> chat, commands;
    private ConfigurationBot configurationBot;
    private boolean running;
    private Model model;
    private String[] key = null;

    public Bot(){
        configurationBot = new ConfigurationBot(IDAPP, IDSECRET);
    }

    public Bot(String _OauthKey, ArrayList<Class> listaComandi, String channelNameField, Model model) {

        if (!new File(System.getProperty("user.home") + "/settingsIziD.json").exists()) {
            try {
                key = _OauthKey.split(":");
                this._OauthKey = key[1];
            } catch (NullPointerException e) {
                System.err.println("IMPOSTAZIONI > TWITCH");
            }

            twitchClient = TwitchClientBuilder.init()
                    .withClientId(IDAPP)
                    .withClientSecret(IDSECRET)
                    .withCredential(key[1])
                    .build();
//        twitchClient.getCredentialManager().getOAuthTwitch().requestPermissionsFor("chat", CHAT_LOGIN);ù
            configurationBot = new ConfigurationBot(twitchClient, _OauthKey, IDAPP, IDSECRET, channelNameField);
            configurationBot.setID(IDAPP);
            configurationBot.setSecretID(IDSECRET);
            configurationBot.setChannelName(channelNameField);
            twitchClient.connect();
            twitchClient.getDispatcher().registerListener(this);
            running = true;
            model.setSTATUS(1, true);
            } else {
                configurationBot = load(null);
                twitchClient = TwitchClientBuilder.init()
                        .withClientId(configurationBot.getID())
                        .withClientSecret(configurationBot.getSecretID())
                        .withCredential(configurationBot.getOauth())
                        .build();
                configurationBot.setTwitchClient(twitchClient);
                twitchClient.connect();
                twitchClient.getDispatcher().registerListener(this);
                running = true;
            }

        chat = new ArrayList<String>();
        commands = new ArrayList<String>();
        this.model = model;
        this.listaComandi = listaComandi;
    }

    public ConfigurationBot load(TwitchClient twitchClient) {
        try {
            Gson gson = new Gson();
            File load = new File(System.getProperty("user.home") + "/settingsIziD.json");
            FileReader fileReader = new FileReader(load);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String json = bufferedReader.readLine();
            HashMap<String, String> loadList = gson.fromJson(json, new TypeToken<HashMap<String,String>>() {}.getType());
            return configurationBot = new ConfigurationBot(twitchClient, loadList.get("OAUTH"), loadList.get("ID"),
                    loadList.get("IDSECRET"), loadList.get("CHNAME"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendMessage(String msg) {
        twitchClient.getMessageInterface().sendMessage(configurationBot.getChannelName(), msg);
    }

    /**
     * Method to register all commands
     */
    public void registerCommands(BotTriggerUI controller, ArrayList<Class> comandi) {
        // INIZIALIZZO I COMANDI ALL'AVVIO DEL BOT
        Crea crea = new Crea();
        crea.init("crea", model.getListaPlayerRegistrati(), comandi, model);
        Partecipa partecipa = new Partecipa();
        partecipa.init("combatti", model.getListaPlayerRegistrati(), comandi,
                model, controller, model.getListaPlayerJoinati());
        Compra compra = new Compra();
        compra.init("compra", model.getListaPlayerRegistrati(), model);
        Diamanti diamanti = new Diamanti();
        diamanti.init("diamanti", model.getListaPlayerRegistrati(), model);
        Regolamento regolamento = new Regolamento();
        twitchClient.getCommandHandler().registerCommand(crea);
        twitchClient.getCommandHandler().registerCommand(partecipa);
        twitchClient.getCommandHandler().registerCommand(compra);
        twitchClient.getCommandHandler().registerCommand(diamanti);
        twitchClient.getCommandHandler().registerCommand(regolamento);

        for (Class c : listaComandi) {
            twitchClient.getCommandHandler().registerCommand(c);
        }
    }

    public boolean isLive() {
        StreamEndpoint streamEndpoint = twitchClient.getStreamEndpoint();

        if (streamEndpoint.isLive(configurationBot.getChannel())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isRunning() {
        return running;
    }

    /**
     * Method to register all features
     */
    public void registerFeature() {


    }

    private void loadConfiguration() {

    }

    public void stopBot() {
        twitchClient.disconnect();
        running = false;
    }

    public void reconnectBot() {
        twitchClient.connect();
        running = true;
    }

    public void joinChannel() {
        channelEndPoint = twitchClient.getChannelEndpoint();
        channel = channelEndPoint.getChannel(configurationBot.getChannelName());
        if (configurationBot.getChannel() == null) {
            configurationBot.setChannel(configurationBot.getChannelName());
        }
        twitchClient.getMessageInterface().joinChannel(configurationBot.getChannelName());
    }

    public void startBot() {
        twitchClient.connect();
        twitchClient.getDispatcher().registerListener(this);
        running = true;
    }

    public ConfigurationBot getConfigurationBot() {
        return configurationBot;
    }

    /*
        @Override
        protected void onMessage(User user, Channel channel, String message) {
            chat.add(user + ": " + message);
        }

        @Override
        protected void onCommand(User user, Channel channel, String command) {
            System.out.println(command);
            if (command.equalsIgnoreCase(listaComandi.get(0))) { //CREA
                botTrigger.signUp(user.toString());
                commands.add(user + ": " + command);
            } else if (command.equalsIgnoreCase(listaComandi.get(1))) { // COMBATTI
                botTrigger.joinInstruction(user.toString());
                commands.add(user + ": " + command);
            }  else if (command.contains(listaComandi.get(1))) { // COMBATTI [VALUE]
                String[] com = command.split(" ");
                System.out.println("[0]: " + com[0] + "\n[1]: " + com[1]);
                try {
                    botTrigger.join(user.toString(), Integer.valueOf(com[1]));
                } catch (NumberFormatException e) {
                    sendMessage("/me " + user + " Occhio inserisci un numero valido BabyRage", channel);
                }
                commands.add(user + ": " + command);
            } else if (command.equalsIgnoreCase(listaComandi.get(2))) { // COMPRA
                botTrigger.compraInstruction(user.toString());
                commands.add(user + ": " + command);
            } else if (command.contains(listaComandi.get(2))) { // COMPRA [VALUE]
                String[] com = command.split(" ");
                System.out.println("[0]: " + com[0] + "\n[1]: " + com[1]);
                botTrigger.compraValue(user.toString(), Integer.valueOf(com[1]));
                commands.add(user + ": " + command);
            } else if (command.equalsIgnoreCase(listaComandi.get(3))) { // DIAMANTI
                botTrigger.showDiamondUser(user.toString());
                commands.add(user + ": " + command);
            } else if (command.equalsIgnoreCase(listaComandi.get(4))) { // REGOLAMENTO
                botTrigger.rules();
                commands.add(user + ": " + command);
            }
        }
    */
    public ArrayList<String> getChat() {
        return chat;
    }
    public void setListaComandiPersonalizzati(ArrayList<Class> listaComandiPersonalizzati) {
        this.listaComandi = listaComandiPersonalizzati;
    }

    public ArrayList<String> getCommands() {
        return commands;
    }
 /*


    public String getUsernameBot() {
        return usernameBot;
    }

    public void setUsernameBot(String usernameBot) {
        usernameBot = usernameBot;
    }

    public void set_OauthKey(String _OauthKey) {
        this._OauthKey = _OauthKey;
    }
    */
BotTrigger botTrigger;
    public void setBotTrigger(BotTrigger botTrigger) {
        this.botTrigger = botTrigger;
    }
}
