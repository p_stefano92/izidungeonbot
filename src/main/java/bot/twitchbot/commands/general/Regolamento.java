package bot.twitchbot.commands.general;

import me.philippheuer.twitch4j.events.event.irc.ChannelMessageEvent;
import me.philippheuer.twitch4j.message.commands.Command;
import me.philippheuer.twitch4j.message.commands.CommandPermission;

public class Regolamento extends Command {

    public Regolamento() {
        super();
        setCommand("regolamento");
        setCategory("general");
        setDescription("Mostra le regole principali");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
    }

    @Override
    public void executeCommand(ChannelMessageEvent messageEvent) {
        super.executeCommand(messageEvent);

        String[] risposta = {"/me Le Regole sono facili",
                "1) Per poter partecipare basta usare il comando !crea così da creare il proprio personaggio. ",
                "2) quando si combatte contro i BOZZ si scommettono i Diamanti.",
                "3) Quando si comprano casse dal mercato la consegna non è immediata date il tempo allo streamer di consegnarvele," +
                        " è sempre buona norma usare !setID [WASDITID] , così che lo streamer conoscerà già il vostro ID senza" +
                        "perdere tempo a chiedervelo.",
                "4) Partecipare è totalmente facoltativo e gratuito WASDIT non è responsabile in alcun modo del progetto" +
                        "è un'idea indipendente creata da Discordia Community che mette in palio le proprie casse ai partecipanti!",
                "5) Se non hai capito il regolamento usa !regolamento. Per altri comandi sono in arrivo con" +
                        " futuri aggiornamenti. Buon Divertimento by IziPeezeLemonSqueeze"};
        for (String r : risposta) {
            sendMessageToChannel(messageEvent.getChannel().getName(), r);
        }
    }

    @Override
    public String toString() {
        return "regolamento";
    }
}
