package bot.twitchbot.commands.general;

import bot.model.Model;
import bot.pg.Player;
import me.philippheuer.twitch4j.events.event.irc.ChannelMessageEvent;
import me.philippheuer.twitch4j.message.commands.Command;
import me.philippheuer.twitch4j.message.commands.CommandPermission;

import java.util.ArrayList;

public class Crea extends Command {

    private Model model;
    private ArrayList<Player> listaPlayerRegistrati;
    private ArrayList<Class> comandi;
    private Player newPlayer;

    public Crea() {
        super();
        setCommand("crea");
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
    }

    public void init(String comando, ArrayList<Player> listaPlayerRegistrati, ArrayList<Class> comandi, Model model) {
        setCommand(comando);
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
        this.listaPlayerRegistrati = listaPlayerRegistrati;
        this.model = model;
        this.comandi = comandi;

    }

    @Override
    public void executeCommand(ChannelMessageEvent messageEvent) {
        super.executeCommand(messageEvent);

        String idTwitch = messageEvent.getUser().getName();
        boolean exist = false;
        newPlayer = new Player(idTwitch, model.getDiamantiNP(), 1, model.getAttaccoNP(), model.getDifesaNP());
        for (int i = 0; i < listaPlayerRegistrati.size(); i++) {
            if (listaPlayerRegistrati.get(i).getIdTwitch().equals(newPlayer.getIdTwitch())) {
                exist = true;
            }
        }
        if  (!exist) {
            System.out.println("NOT EXIST > GO NEW");
            listaPlayerRegistrati.add(0, newPlayer);
            sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + idTwitch +
                    " Benvenuto su IziDungeon, da ora potrai partecipare" +
                    " a questo gioco e vincere casse WASDIT");
            //TODO CONTROLLO LISTA JSON ALTRIMENTI CREALA
        } else {

            sendMessageToChannel(messageEvent.getChannel().getName(),"/me " + idTwitch + " Sei già registato" +
                    " e pronto al combattimento! scrivi " + comandi.get(1).toString() + " per partecipare al combattimento del prossimo BOZZ SeemsGood");
        }
        exist = false;
    }

    @Override
    public String toString() {
        return "crea";
    }
}
