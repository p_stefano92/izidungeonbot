package bot.twitchbot.commands.general;

import bot.model.BotTriggerUI;
import bot.model.Model;
import bot.pg.Player;
import me.philippheuer.twitch4j.events.event.irc.ChannelMessageEvent;
import me.philippheuer.twitch4j.message.commands.Command;
import me.philippheuer.twitch4j.message.commands.CommandPermission;

import java.util.ArrayList;

public class Partecipa extends Command {
    private Model model;
    private ArrayList<Player> listaPlayerRegistrati, listaPlayerJoinati;
    private ArrayList<Class> comandi;
    private String comando;
    private BotTriggerUI botTriggerUI;

    public Partecipa() {
        super();
        setCommand("partecipa");
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
    }

    public void init(String comando, ArrayList<Player> listaPlayerRegistrati, ArrayList<Class> comandi, Model model,
                     BotTriggerUI botTriggerUI, ArrayList<Player> listaPlayerJoinati) {
        setCommand(comando);
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
        this.listaPlayerRegistrati = listaPlayerRegistrati;
        this.listaPlayerJoinati = listaPlayerJoinati;
        this.model = model;
        this.comandi = comandi;
        this.comando = comando;
        this.botTriggerUI = botTriggerUI;
    }

    @Override
    public void executeCommand(ChannelMessageEvent messageEvent) {
        super.executeCommand(messageEvent);
        if (messageEvent.getMessage().equals("!" + comando)) {
            sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() +
                    " scrivi il comando cosi: !" + comando + " [valoreDaScommettere] ad esempio: !" + comando + " 150." +
                    " il valore è quanti diamanti si vogliono mettere sul piatto");
        } else /*(messageEvent.getMessage().contains(comando)) */{
            String [] cm = messageEvent.getMessage().split(" ");
            try {
                int scommessa = Integer.valueOf(cm[1]);
                boolean exist = false;
                boolean alreadyJoined = false;
                if (scommessa > 1) {
                if (!model.getSTATUS(0)) {
                    for (int i = 0; i < listaPlayerRegistrati.size(); i++) {
                        if (listaPlayerRegistrati.get(i).getIdTwitch().equals(messageEvent.getUser().getName())) {
                            exist = true;
                        }
                    }
                    if (exist) {
                        for (int i = 0; i < listaPlayerJoinati.size(); i++) {
                            if (listaPlayerJoinati.get(i).getIdTwitch() == messageEvent.getUser().getName()) {
                                alreadyJoined = true;
                                sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() + " Sei già joinato per affrontare il Bozz, attendi che il combattimento" +
                                        "abbia inizio! PogChamp");
                            }
                        }
                        if (!alreadyJoined) {
                            listaPlayerJoinati.add(model.getFromListaRegistrati(messageEvent.getUser().getName()));
                            sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() + ", tra poco affronterai il Bozz, attendi che il combattimento" +
                                    "abbia inizio! Hai scommesso: " + scommessa + " Diamanti. SeemsGood");
                            listaPlayerRegistrati.get(listaPlayerRegistrati.
                                    indexOf(model.getFromListaRegistrati(messageEvent.getUser().getName()))).setDiamanti(
                                    (listaPlayerRegistrati.get(listaPlayerJoinati.
                                            indexOf(model.getFromListaRegistrati(messageEvent.getUser().getName()))).getDiamanti() - scommessa)
                            );
                            botTriggerUI.updatePartyUI(messageEvent.getUser().getName(), scommessa);
                            //TODO RECUPERO E SALVATAGGIO INFO SUL PLAYER OLTRE AL NOME
                            model.getParty().addOnParty(model.getFromListaRegistrati(messageEvent.getUser().getName()));
                        }
                    } else {
                        sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() +
                                " Non sei ancora registrato su IziDungeon, utilizza il comando " + comandi.get(0).toString() +
                                " per poter partecipare a questo gioco e vincere casse WASDIT");
                    }
                } else {
                    sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() +
                            ", Non è ancora tempo di combattere il boss, attendi che tutti si preparino! SwiftRage");
                }
            } else {
                    sendMessageToChannel(messageEvent.getChannel().getName(), "/me Scommessa non valida");
                }
            } catch (NumberFormatException e) {
                sendMessageToChannel(messageEvent.getChannel().getName(), "/me Inserisci una cifra: " + comandi.get(1).toString() + "123");
            }
            //TODO AGGIUNGERE STATO COMBATTIMENTO INIZIATO
        }
    }

    @Override
    public String toString() {
        return "partecipa";
    }
}
