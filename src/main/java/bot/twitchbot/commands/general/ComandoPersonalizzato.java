package bot.twitchbot.commands.general;

import me.philippheuer.twitch4j.events.event.irc.ChannelMessageEvent;
import me.philippheuer.twitch4j.message.commands.Command;
import me.philippheuer.twitch4j.message.commands.CommandPermission;

public class ComandoPersonalizzato extends Command {

    private String comandoPersonalizzato, rispostaComando;

    public ComandoPersonalizzato(String comandoPersonalizzato, String rispostaComando) {
        super();
        this.comandoPersonalizzato = comandoPersonalizzato;
        this.rispostaComando = rispostaComando;
        setCommand(comandoPersonalizzato);
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
    }


    @Override
    public void executeCommand(ChannelMessageEvent messageEvent) {
        super.executeCommand(messageEvent);
        sendMessageToChannel(messageEvent.getChannel().getName(), rispostaComando);
    }

    @Override
    public String toString() {
        return comandoPersonalizzato;
    }
}
