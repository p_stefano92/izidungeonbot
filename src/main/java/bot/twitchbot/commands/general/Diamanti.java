package bot.twitchbot.commands.general;

import bot.model.Model;
import bot.pg.Player;
import com.sun.org.apache.xpath.internal.operations.Mod;
import me.philippheuer.twitch4j.events.event.irc.ChannelMessageEvent;
import me.philippheuer.twitch4j.message.commands.Command;
import me.philippheuer.twitch4j.message.commands.CommandPermission;

import java.util.ArrayList;

public class Diamanti extends Command {

    private Model model;
    private String comando;
    private ArrayList<Player> listaPlayerRegistrati;

    public Diamanti() {
        super();
        setCommand("diamanti");
        setCategory("general");
        setDescription("Mostra le regole principali");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
    }

    public void init(String comando, ArrayList<Player> listaPlayerRegistrati, Model model) {
        setCommand(comando);
        setCategory("general");
        setDescription("Mostra le regole principali");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");

        this.comando = comando;
        this.listaPlayerRegistrati = listaPlayerRegistrati;
        this.model = model;
    }

    @Override
    public void executeCommand(ChannelMessageEvent messageEvent) {
        super.executeCommand(messageEvent);
        boolean exist = false;
        for (int i = 0; i < listaPlayerRegistrati.size(); i++) {
            if (listaPlayerRegistrati.get(i).getIdTwitch().equals(messageEvent.getUser().getName())) {
                exist = true;
            }
        }
        if (exist) {
            sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() +
                    " I tuoi averi ammontano a: " +
                    listaPlayerRegistrati.get(listaPlayerRegistrati.indexOf(model.getFromListaRegistrati(messageEvent.getUser().getName()))).getDiamanti());
        } else {
            sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() +
                    " Ops! forse devi prima creare un personagggio!");
        }
    }

    @Override
    public String toString() {
        return "diamanti";
    }
}
