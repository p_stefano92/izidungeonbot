package bot.twitchbot.commands.general;

import bot.model.Model;
import bot.pg.Player;
import me.philippheuer.twitch4j.events.event.irc.ChannelMessageEvent;
import me.philippheuer.twitch4j.message.commands.Command;
import me.philippheuer.twitch4j.message.commands.CommandPermission;

import java.util.ArrayList;

public class Compra extends Command {

    private Model model;
    private String comando;
    private ArrayList<Player> listaPlayerRegistrati;
    private int value;

    public Compra() {
        super();
        setCommand("compra");
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
    }

    public void init(String comando, ArrayList<Player> listaPlayerRegistrati, Model model) {

        setCommand(comando);
        setCategory("general");
        setDescription("Comando generato dallo streamer");
        getRequiredPermissions().add(CommandPermission.EVERYONE);
        setUsageExample("");
        this.listaPlayerRegistrati = listaPlayerRegistrati;
        this.model = model;
        this.comando = comando;
        value = 0;
    }

    @Override
    public void executeCommand(ChannelMessageEvent messageEvent) {
        super.executeCommand(messageEvent);
        boolean exist = false;
        for (int i = 0; i < listaPlayerRegistrati.size(); i++) {
            if (listaPlayerRegistrati.get(i).getIdTwitch().equals(messageEvent.getUser().getName())) {
                exist = true;
            }
        }
        if (exist) {
            try {
                String[] cm = comando.split(" ");
                try {
                    value = Integer.valueOf(cm[1]);
                } catch (NumberFormatException e) {
                    sendMessageToChannel(messageEvent.getChannel().getName(),
                            "/me " + messageEvent.getUser().getName() + " Inserisci un valore a cifre: 123");
                }
                if ((listaPlayerRegistrati.get(listaPlayerRegistrati.
                        indexOf(model.getFromListaRegistrati(messageEvent.getUser().getName()))).getDiamanti() >= (model.getValoreChest() * value)) &&
                        (value <= model.getQntChest()) && (value >= 1)) {
                    listaPlayerRegistrati.get(listaPlayerRegistrati.indexOf(model.getFromListaRegistrati(messageEvent.getUser().getName()))).setDiamanti(
                            listaPlayerRegistrati.get(listaPlayerRegistrati.indexOf(model.getFromListaRegistrati(messageEvent.getUser().getName()))).getDiamanti() - (model.getValoreChest() * value));
                    //TODO MANDARE ALLO STREAMER LA QUANTITA DI CASSE COMPRATE DALL'UTENTE
                } else {
                    sendMessageToChannel(messageEvent.getChannel().getName(),
                            "/me " + messageEvent.getUser().getName() + " Per poter comprare le casse devi prima creare un pg!");
                }
                sendMessageToChannel(messageEvent.getChannel().getName(),
                        "/me " + messageEvent.getUser().getName() + " Congratulazioni, hai appena acquistato " + value + " casse" +
                                " di WASDIT, vai ad aprirle e dicci cosa trovi!");
            } catch (Exception e) {
                sendMessageToChannel(messageEvent.getChannel().getName(),
                        "/me " + messageEvent.getUser().getName() + " Scrivi: comando valore , il valore fa riferimento alla quantità di casse che si voglio acquistare");
            } finally {
         /*   sendMessageToChannel(messageEvent.getChannel().getName(),
                    "/me " + user + " Congratulazioni, hai appena acquistato " + value + " casse" +
                    " di WASDIT, vai ad aprirle e dicci cosa trovi!"); */
            }
        } else {
            sendMessageToChannel(messageEvent.getChannel().getName(), "/me " + messageEvent.getUser().getName() +
                    " Ops! forse devi prima creare un personagggio!");
        }
    }

    @Override
    public String toString() {
        return "compra";
    }
}
