package bot.controller;

import animatefx.animation.*;
import bot.model.*;
import bot.pg.Boss;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.application.Platform;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class Controller  implements SettingsTrigger, BossTrigger, BotTriggerUI {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextArea chatArea;

    @FXML
    private Label labelQntChest;

    @FXML
    private Label labelCostoChest;

    @FXML
    private Label labelDiamonStart;

    @FXML
    private Label labelAttackStart;

    @FXML
    private Label labelDefenceStart;

    @FXML
    private Label labelValueJoin;

    @FXML
    private Label labelDifficolta;

    @FXML
    private JFXProgressBar barBossHealt;

    @FXML
    private JFXProgressBar barPartyHealt;

    @FXML
    private Label labelBossName;

    @FXML
    private Label labelBossType;

    @FXML
    private Label labelBossLv;

    @FXML
    private Label labelBossAttack;

    @FXML
    private Label labelBossAttackBuffed;

    @FXML
    private Label labelBossDefence;

    @FXML
    private Label labelBossDefenceBuffed;

    @FXML
    private Label labelPartyBuff;

    @FXML
    private Label labelBossBuff;

    @FXML
    private Label labelBossHealt;

    @FXML
    private Label labelPartyHealt;

    @FXML
    private Label labelPartyDamage;

    @FXML
    private Label labelPartyDefence;

    @FXML
    private Label labelPartyWinner;

    @FXML
    private Label labelPartyNumber;

    @FXML
    private JFXButton buttonSetting;

    @FXML
    private JFXButton buttonConnect;

    @FXML
    private MaterialDesignIconView iconPlayStopBot;

    private boolean[] requestCheck = {false, false};

    private Model model;
    public void setModel(Model model) {
        this.model = model;
        labelQntChest.setText(String.valueOf(model.getQntChest()));
        labelCostoChest.setText(String.valueOf(model.getValoreChest()));
        model.setBossTrigger((BossTrigger) this);
        model.setBotTriggerUI((BotTriggerUI) this);
    }

    @FXML
    void doOpenSetting(MouseEvent event) {
        new JackInTheBox(buttonSetting).play();
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("Settings.fxml"));
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner((Stage) ((Node)event.getSource()).getScene().getWindow());
        try {
            Scene scene = new Scene((AnchorPane)loader.load());
            ControllerSettings controllerSettings = loader.getController();
            controllerSettings.setController(this);
            controllerSettings.setSettingsTrigger(this);
            controllerSettings.setModel(model);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.showAndWait();
    }

    public void updateChestUI() {
        labelQntChest.setText(String.valueOf(model.getQntChest()));
        labelCostoChest.setText(String.valueOf(model.getValoreChest()));
        labelDiamonStart.setText(String.valueOf(model.getDiamantiNP()));
        labelAttackStart.setText(String.valueOf(model.getAttaccoNP()));
        labelDefenceStart.setText(String.valueOf(model.getDifesaNP()));
        labelValueJoin.setText(String.valueOf(model.getDurataJoin()));
        if (model.getRadiocheckDifficolta()[0]) {
            labelDifficolta.setText("Facile lv 1 - 10");
        }
        if (model.getRadiocheckDifficolta()[1]) {
            labelDifficolta.setText("Medio lv 10 - 20");
        }
        if (model.getRadiocheckDifficolta()[2]) {
            labelDifficolta.setText("Difficile lv 20 - 40");
        }
        if (model.getRadiocheckDifficolta()[3]) {
            labelDifficolta.setText("Ingiusto lv 40 - 99");
        }
    }

    public void initBoss(int index) {
        labelBossName.setText(model.getListaBossRun().get(index).getNome());
        labelBossLv.setText(String.valueOf(model.getListaBossRun().get(index).getLivello()));
        labelBossType.setText(model.getListaBossRun().get(index).getTipo());
        labelBossAttack.setText(String.valueOf(model.getListaBossRun().get(index).getAttacco()));
        labelBossAttackBuffed.setText(String.valueOf(model.getListaBossRun().get(index).getAttaccoBuffato()));
        labelBossDefence.setText(String.valueOf(model.getListaBossRun().get(index).getDifesa()));
        labelBossDefenceBuffed.setText(String.valueOf(model.getListaBossRun().get(index).getDifesaBuffata()));
        //TODO IMPOSTARE I LABEL PER I BUFF
        barBossHealt.setProgress(100);
        labelBossHealt.setText(String.valueOf(model.getListaBossRun().get(index).getHP())
                + "/" + String.valueOf(model.getListaBossRun().get(index).getHP()));

    }
    private int tempo;
    private Thread threadJoin;
    public Runnable startJoin() {
        threadJoin = new Thread(new Runnable() {
            public void run() {
                synchronized (this) {
                    tempo = model.getDurataJoin();
                    System.out.println(tempo);
                    model.setSTATUS(0,true);
                //    model.setSTATUS(1, true);
                    while (tempo != 0) {
                        System.out.println(tempo);
                        notifyAll();
                        try {
                            wait(1000);
                            Platform.runLater(new Runnable() {
                                public void run() {
                                    labelValueJoin.setText(String.valueOf(tempo));
                                }
                            });
                            tempo--;
                            if (tempo == (model.getDurataJoin() / 2)) {
                                model.getBot().sendMessage("/me Rimangono " + tempo + "secondi per poter" +
                                        " joinare al combattimento");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    model.getBot().sendMessage("/me Il tempo per joinare è scaduto ora inizia il" +
                            " combattimento!");
                    model.setSTATUS(0, false);
                    model.setSTATUS(2, true);
                }
            }
        });
        return threadJoin;
    }

    @FXML
    void doConnect(MouseEvent event) {

        if (model.getBot() == null) {
            new JackInTheBox(iconPlayStopBot).play();
            model.connectBot();
            Platform.runLater(new Runnable() {
                public void run() {
                    iconPlayStopBot.setGlyphName("STOP");
                }
            });
        } else if (iconPlayStopBot.getGlyphName().equals("STOP") && model.getBot().isRunning()) {
            model.getBot().sendMessage("/me " + model.getNomeBot() + " disconesso!");
            model.getBot().stopBot();
            iconPlayStopBot.setGlyphName("PLAY");
        } else if (iconPlayStopBot.getGlyphName().equals("PLAY") && !model.getBot().isRunning() &&
            model.getBot().getConfigurationBot().getTwitchClient() != null) {
            iconPlayStopBot.setGlyphName("STOP");
            model.getBot().reconnectBot();
            model.getBot().sendMessage("/me " + model.getNomeBot() + " riconnesso!");
        }
    }

    private void initThreadChat() {
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    wait(1000);
                    for (String chat : model.getBotChat()) {
                        chatArea.appendText(chat);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }
    //DEBUG
    private int partyNumber = 0, partyHealt = 100, partyDefence, partyAttack, piattoDiamanti;
    public void updatePartyUI(String user, int scommessa) {
        //DEBUG
        partyNumber++;
        partyHealt = (partyHealt * partyNumber);
        partyDefence = (partyNumber * model.getDifesaNP());
        partyAttack = (partyNumber * model.getAttaccoNP());
        piattoDiamanti += scommessa;

        Platform.runLater(new Runnable() {
            public void run() {
                labelPartyNumber.setText(String.valueOf(partyNumber));
                labelPartyHealt.setText(String.valueOf(partyHealt));
                labelPartyDamage.setText(String.valueOf(partyAttack));
                labelPartyDefence.setText(String.valueOf(partyDefence));
                labelPartyWinner.setText(String.valueOf(piattoDiamanti));
                //TODO AGGIUGERE PIATTO DIAMANTI
            }
        });
    }

    public JFXButton getButtonConnect() {
        return buttonConnect;
    }

    @FXML
    void initialize() {
        assert chatArea != null : "fx:id=\"chatArea\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelQntChest != null : "fx:id=\"labelQntChest\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelCostoChest != null : "fx:id=\"labelCostoChest\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelDiamonStart != null : "fx:id=\"labelDiamonStart\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelAttackStart != null : "fx:id=\"labelAttackStart\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelDefenceStart != null : "fx:id=\"labelDefenceStart\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelValueJoin != null : "fx:id=\"labelValueJoin\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelDifficolta != null : "fx:id=\"labelDifficolta\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert barBossHealt != null : "fx:id=\"barBossHealt\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert barPartyHealt != null : "fx:id=\"barPartyHealt\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossName != null : "fx:id=\"labelBossName\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossType != null : "fx:id=\"labelBossType\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossLv != null : "fx:id=\"labelBossLv\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossAttack != null : "fx:id=\"labelBossAttack\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossAttackBuffed != null : "fx:id=\"labelBossAttackBuffed\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossDefence != null : "fx:id=\"labelBossDefence\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossDefenceBuffed != null : "fx:id=\"labelBossDefenceBuffed\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelPartyBuff != null : "fx:id=\"labelPartyBuff\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossBuff != null : "fx:id=\"labelBossBuff\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelBossHealt != null : "fx:id=\"labelBossHealt\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelPartyHealt != null : "fx:id=\"labelPartyHealt\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelPartyDamage != null : "fx:id=\"labelPartyDamage\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelPartyDefence != null : "fx:id=\"labelPartyDefence\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelPartyWinner != null : "fx:id=\"labelPartyWinner\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert labelPartyNumber != null : "fx:id=\"labelPartyNumber\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert buttonSetting != null : "fx:id=\"buttonSetting\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert buttonConnect != null : "fx:id=\"buttonConnect\" was not injected: check your FXML file 'IziDungeon.fxml'.";
        assert iconPlayStopBot != null : "fx:id=\"iconPlayStopBot\" was not injected: check your FXML file 'IziDungeon.fxml'.";

        if (!new File(System.getProperty("user.home") + "/settingsIziD.json").exists()) {
            buttonConnect.setDisable(true);
            buttonConnect.setTooltip(new Tooltip("Vai prima nelle impostazioni!"));
        } else {
            buttonConnect.setDisable(false);
        }
        //TODO RICORDARSI DI RESETTARE I FIELD CON L'ACCESSO A TWTICH
        Platform.runLater(new Runnable() {
            public void run() {
                labelDiamonStart.setText(String.valueOf(model.getGeneralSettings().get("DIAMNP")));
                labelAttackStart.setText(String.valueOf(model.getGeneralSettings().get("ATKNP")));
                labelDefenceStart.setText(String.valueOf(model.getGeneralSettings().get("DEFNP")));
                labelValueJoin.setText(String.valueOf(model.getGeneralSettings().get("DJOIN")));
                if (model.getRadiocheckDifficolta()[0]) {
                    labelDifficolta.setText("Facile lv 1 - 10");
                }
                if (model.getRadiocheckDifficolta()[1]) {
                    labelDifficolta.setText("Medio lv 10 - 20");
                }
                if (model.getRadiocheckDifficolta()[2]) {
                    labelDifficolta.setText("Difficile lv 20 - 40");
                }
                if (model.getRadiocheckDifficolta()[3]) {
                    labelDifficolta.setText("Ingiusto lv 40 - 99");
                }
            }
        });

    }
}

