package bot.controller;

import animatefx.animation.FadeIn;
import animatefx.animation.Hinge;
import animatefx.animation.Pulse;
import bot.jsonsave.Salvataggio;
import bot.model.Model;
import bot.model.SettingsTrigger;
import bot.settings.SettingsGeneral;
import bot.twitchbot.commands.general.*;

import com.google.gson.Gson;
import com.google.common.reflect.TypeToken;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ControllerSettings {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton buttonGenerale;

    @FXML
    private JFXButton buttonTwitch;

    @FXML
    private JFXButton buttonComandi;

    @FXML
    private JFXButton buttonInfo;

    @FXML
    private JFXButton buttonEsci;

    @FXML
    private AnchorPane twitchPane;

    @FXML
    private JFXButton buttonSalvaTwitch;

    @FXML
    private JFXTextField fieldNomeBot;

    @FXML
    private JFXTextField fieldOauth;

    @FXML
    private JFXTextField fieldNomeCanale;

    @FXML
    private MaterialDesignIconView helpKeyOauth;

    @FXML
    private AnchorPane comandiPane;

    @FXML
    private JFXButton buttonSalvaComandi;

    @FXML
    private JFXTextField fieldComandiCrea;

    @FXML
    private JFXTextField fieldComandiPartecipa;

    @FXML
    private JFXTextField fieldComandiCompra;

    @FXML
    private JFXTextField fieldComandiDiamanti;

    @FXML
    private JFXTextField fieldComandiRegolamento;

    @FXML
    private AnchorPane generalePane;

    @FXML
    private JFXTextField fieldQuantitaChest;

    @FXML
    private JFXTextField fieldCostoChest;

    @FXML
    private JFXTextField fieldtempoJoin;

    @FXML
    private JFXTextField fieldDiamantiNP;

    @FXML
    private JFXTextField fieldAttackNP;

    @FXML
    private JFXTextField fieldDefenceNP;

    @FXML
    private JFXButton buttonSalvaGenerale;

    @FXML
    private JFXRadioButton radioFacile;

    @FXML
    private JFXRadioButton radioMedio;

    @FXML
    private JFXRadioButton radioDifficile;

    @FXML
    private JFXRadioButton radioIngiusto;

    @FXML
    private VBox boxInfoSettings;

    private Controller controller;
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @FXML
    void doComandi(MouseEvent event) {
        if (!comandiPane.isVisible()) {
            comandiPane.setVisible(true);
            new FadeIn(comandiPane).play();
            twitchPane.setVisible(false);
            generalePane.setVisible(false);
            boxInfoSettings.setVisible(false);
        }
    }

    @FXML
    void doSalvaComandi(MouseEvent event) {
        ArrayList<Class> comandi = new ArrayList<Class>();
        Crea crea = new Crea();
        crea.init(fieldComandiCrea.getText(), model.getListaPlayerRegistrati(), comandi, model);
        Partecipa partecipa = new Partecipa();
        partecipa.init(fieldComandiPartecipa.getText(), model.getListaPlayerRegistrati(), comandi,
                model, controller, model.getListaPlayerJoinati());
        Compra compra = new Compra();
        compra.init(fieldComandiCompra.getText(), model.getListaPlayerRegistrati(), model);
        Diamanti diamanti = new Diamanti();
        diamanti.init(fieldComandiDiamanti.getText(), model.getListaPlayerRegistrati(), model);
        Regolamento regolamento = new Regolamento();

        comandi.add(0, crea.getClass());
        comandi.add(1, partecipa.getClass());
        comandi.add(2, compra.getClass());
        comandi.add(3, diamanti.getClass());
        comandi.add(4, regolamento.getClass());
        model.setComandiPersonalizzati(comandi);
    }

    @FXML
    void doEsci(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.close();
    }

    //TODO PRENDI I DATI DAL SALVATAGGIO JSON SE NON CI SONO INIZIALIZZI CON I DEFAULT
    private boolean existGeneralSettings;
    private HashMap<String, Integer> generalSettings;
    @FXML
    void doGenerale(MouseEvent event) {
        if (!generalePane.isVisible()) {
            generalePane.setVisible(true);
            new FadeIn(generalePane).play();
            twitchPane.setVisible(false);
            comandiPane.setVisible(false);
            boxInfoSettings.setVisible(false);
        }
        if (!new File(System.getProperty("user.home") + "/settingsGeneralIziD.json").exists()) {
            File saveGeneral = new File(System.getProperty("user.home") + "/settingsGeneralIziD.json");
                try {
                    Gson gson = new Gson();
                    generalSettings = new HashMap<String, Integer>();
                    generalSettings.put("QNTCHEST", 5);
                    generalSettings.put("VCHEST", 1);
                    generalSettings.put("DJOIN", 240);
                    generalSettings.put("DIAMNP", 1000);
                    generalSettings.put("ATKNP", 10);
                    generalSettings.put("DEFNP", 5);
                    generalSettings.put("DIFF", 0);
                    Salvataggio save = new Salvataggio(generalSettings);
                    String json = gson.toJson(save.getGeneralSettings(), new TypeToken<HashMap<String, Integer>>() {}.getType());
                    saveGeneral.createNewFile();
                    FileWriter fw = new FileWriter(saveGeneral);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(json);
                    bw.flush();
                    bw.close();
                //    existGeneralSettings = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
        } else {
            try {
                Gson gson = new Gson();
                File loadGeneral = new File(System.getProperty("user.home") + "/settingsGeneralIziD.json");
                FileReader fr = new FileReader(loadGeneral);
                BufferedReader br = new BufferedReader(fr);
                String json = br.readLine();
                HashMap<String, Integer> hm = gson.fromJson(json, new TypeToken<HashMap<String, Integer>>() {}.getType());
                generalSettings = hm;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            fieldQuantitaChest.setText(String.valueOf(generalSettings.get("QNTCHEST")));
            fieldCostoChest.setText(String.valueOf(generalSettings.get("VCHEST")));
            fieldtempoJoin.setText(String.valueOf(generalSettings.get("DJOIN")));
            fieldDiamantiNP.setText(String.valueOf(generalSettings.get("DIAMNP")));
            fieldAttackNP.setText(String.valueOf(generalSettings.get("ATKNP")));
            fieldDefenceNP.setText(String.valueOf(generalSettings.get("DEFNP")));

            if (generalSettings.get("DIFF").equals(0)) {
                radioFacile.setSelected(true);
            }
            if (generalSettings.get("DIFF").equals(1)) {
                radioMedio.setSelected(true);
            }
            if (generalSettings.get("DIFF").equals(2)) {
                radioDifficile.setSelected(true);
            }
            if (generalSettings.get("DIFF").equals(3)) {
                radioIngiusto.setSelected(true);
            }
        } catch (Exception e) {
            System.err.println("Primo avvio?");
        }
    }


    @FXML
    void doRadioDifficile(MouseEvent event) {
        if (!radioDifficile.isArmed()) {
            radioFacile.setSelected(false);
            radioMedio.setSelected(false);
            radioDifficile.setSelected(true);
            radioIngiusto.setSelected(false);
        }
    }

    @FXML
    void doRadioIngiusto(MouseEvent event) {
        if (!radioIngiusto.isArmed()) {
            radioFacile.setSelected(false);
            radioMedio.setSelected(false);
            radioDifficile.setSelected(false);
            radioIngiusto.setSelected(true);
        }
    }

    @FXML
    void doRadioMedio(MouseEvent event) {
        if (!radioMedio.isArmed()) {
            radioFacile.setSelected(false);
            radioMedio.setSelected(true);
            radioDifficile.setSelected(false);
            radioIngiusto.setSelected(false);
        }
    }

    @FXML
    void doRadioFacile(MouseEvent event) {
        if (!radioFacile.isArmed()) {
            radioFacile.setSelected(true);
            radioMedio.setSelected(false);
            radioDifficile.setSelected(false);
            radioIngiusto.setSelected(false);
        }
    }

    private SettingsTrigger settingsTrigger;
    public void setSettingsTrigger(SettingsTrigger settingsTrigger) {
        this.settingsTrigger = settingsTrigger;
    }

    @FXML
    void doSalvaGenerale(MouseEvent event) {
        if (Integer.valueOf(fieldQuantitaChest.getText()) > -1) {
            model.setQntChest(Integer.valueOf(fieldQuantitaChest.getText()));
        } else {
            fieldQuantitaChest.setText("0");
            model.setQntChest(0);
        }
        if (Integer.valueOf(fieldCostoChest.getText()) > 0) {
            model.setValoreChest(Integer.valueOf(fieldCostoChest.getText()));
        } else {
            fieldCostoChest.setText("1");
            model.setValoreChest(1);
        }
        if (Integer.valueOf(fieldtempoJoin.getText()) > 10) {
            model.setDurataJoin(Integer.valueOf(fieldtempoJoin.getText()));
        } else {
            fieldtempoJoin.setText("10");
            model.setDurataJoin(10);
        }
        if (Integer.valueOf(fieldDiamantiNP.getText()) > -1) {
            model.setDiamantiNP(Integer.valueOf(fieldDiamantiNP.getText()));
        } else {
            fieldDiamantiNP.setText("0");
            model.setDiamantiNP(0);
        }
        if (Integer.valueOf(fieldAttackNP.getText()) > 0) {
            model.setAttaccoNP(Integer.valueOf(fieldAttackNP.getText()));
        } else {
            fieldAttackNP.setText("1");
            model.setAttaccoNP(1);
        }
        if (Integer.valueOf(fieldDefenceNP.getText()) > 0) {
            model.setDifesaNP(Integer.valueOf(fieldDefenceNP.getText()));
        } else {
            fieldDefenceNP.setText("1");
            model.setDifesaNP(1);
        }

        boolean[] radioCheck = {radioFacile.isSelected(), radioMedio.isSelected(),
                radioDifficile.isSelected(), radioIngiusto.isSelected()};
        model.setRadiocheckDifficolta(radioCheck);
        settingsTrigger.updateChestUI();
        model.replaceGeneralSettings();
    }

    @FXML
    void doTwitch(MouseEvent event) {
        if (!twitchPane.isVisible()) {
            twitchPane.setVisible(true);
            new FadeIn(twitchPane).play();
            comandiPane.setVisible(false);
            generalePane.setVisible(false);
            boxInfoSettings.setVisible(false);
        }
        if (new File(System.getProperty("user.home") + "/settingsIziD.json").exists() && fieldNomeCanale.getText() != "" && fieldOauth.getText() != "") {
            Platform.runLater(new Runnable() {
                public void run() {
                    fieldNomeCanale.setText(model.loadInitialUiConfigurationTwitch().get("CHNAME"));
                    fieldOauth.setText(model.loadInitialUiConfigurationTwitch().get("OAUTH"));
                }
            });
        }
    }

    @FXML
    void doHelpKeyOauth(MouseEvent event) {
        try {
            Runtime rt = Runtime.getRuntime();
            rt.exec("rundll32 url.dll,FileProtocolHandler http://twitchapps.com/tmi/");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void doSalvaTwitch(MouseEvent event) {
        model.set_OauthKey(fieldOauth.getText());
        model.setNomeBot(fieldNomeBot.getText());
        model.setNomeCanale(fieldNomeCanale.getText());
        controller.getButtonConnect().setDisable(false);
        model.replaceTwitchConfiguration();
    }


    private Model model;
    public void setModel(Model model) {
        this.model = model;
    }

    @FXML
    void doInfo(MouseEvent event) {

    }
    @FXML
    void initialize() {
        assert buttonGenerale != null : "fx:id=\"buttonGenerale\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonTwitch != null : "fx:id=\"buttonTwitch\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonComandi != null : "fx:id=\"buttonComandi\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonInfo != null : "fx:id=\"buttonInfo\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonEsci != null : "fx:id=\"buttonEsci\" was not injected: check your FXML file 'Settings.fxml'.";
        assert twitchPane != null : "fx:id=\"twitchPane\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonSalvaTwitch != null : "fx:id=\"buttonSalvaTwitch\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldNomeBot != null : "fx:id=\"fieldNomeBot\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldOauth != null : "fx:id=\"fieldOauth\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldNomeCanale != null : "fx:id=\"fieldNomeCanale\" was not injected: check your FXML file 'Settings.fxml'.";
        assert helpKeyOauth != null : "fx:id=\"helpKeyOauth\" was not injected: check your FXML file 'Settings.fxml'.";
        assert comandiPane != null : "fx:id=\"comandiPane\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonSalvaComandi != null : "fx:id=\"buttonSalvaComandi\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldComandiCrea != null : "fx:id=\"fieldComandiCrea\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldComandiPartecipa != null : "fx:id=\"fieldComandiPartecipa\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldComandiCompra != null : "fx:id=\"fieldComandiCompra\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldComandiDiamanti != null : "fx:id=\"fieldComandiDiamanti\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldComandiRegolamento != null : "fx:id=\"fieldComandiRegolamento\" was not injected: check your FXML file 'Settings.fxml'.";
        assert generalePane != null : "fx:id=\"generalePane\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldQuantitaChest != null : "fx:id=\"fieldQuantitaChest\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldCostoChest != null : "fx:id=\"fieldCostoChest\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldtempoJoin != null : "fx:id=\"fieldtempoJoin\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldDiamantiNP != null : "fx:id=\"fieldDiamantiNP\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldAttackNP != null : "fx:id=\"fieldAttackNP\" was not injected: check your FXML file 'Settings.fxml'.";
        assert fieldDefenceNP != null : "fx:id=\"fieldDefenceNP\" was not injected: check your FXML file 'Settings.fxml'.";
        assert buttonSalvaGenerale != null : "fx:id=\"buttonSalvaGenerale\" was not injected: check your FXML file 'Settings.fxml'.";
        assert radioFacile != null : "fx:id=\"radioFacile\" was not injected: check your FXML file 'Settings.fxml'.";
        assert radioMedio != null : "fx:id=\"radioMedio\" was not injected: check your FXML file 'Settings.fxml'.";
        assert radioDifficile != null : "fx:id=\"radioDifficile\" was not injected: check your FXML file 'Settings.fxml'.";
        assert radioIngiusto != null : "fx:id=\"radioIngiusto\" was not injected: check your FXML file 'Settings.fxml'.";
        assert boxInfoSettings != null : "fx:id=\"boxInfoSettings\" was not injected: check your FXML file 'Settings.fxml'.";

        fieldQuantitaChest.setOnKeyReleased((e) -> {
            if (Integer.valueOf(fieldQuantitaChest.getText()) <= 0) {
                fieldQuantitaChest.setText("0");
            }
        });
        fieldCostoChest.setOnKeyReleased((e) -> {
            if (Integer.valueOf(fieldCostoChest.getText()) <= 0) {
                fieldCostoChest.setText("1");
            }
        });
        fieldtempoJoin.setOnKeyReleased((e) -> {
            if (Integer.valueOf(fieldtempoJoin.getText()) <= 10) {
                fieldtempoJoin.setText("10");
            }
        });
        fieldDiamantiNP.setOnKeyReleased((e) -> {
            if (Integer.valueOf(fieldDiamantiNP.getText()) <= -1) {
                fieldDiamantiNP.setText("0");
            }
        });
        fieldAttackNP.setOnKeyReleased((e) -> {
            if (Integer.valueOf(fieldAttackNP.getText()) <= 0) {
                fieldAttackNP.setText("1");
            }
        });
        fieldDefenceNP.setOnKeyReleased((e) -> {
            if (Integer.valueOf(fieldDefenceNP.getText()) <= 0) {
                fieldDefenceNP.setText("1");
            }
        });
        twitchPane.setVisible(false);
        comandiPane.setVisible(false);
        generalePane.setVisible(false);
        fieldNomeBot.setText("IziDungeonBot");
        radioFacile.setSelected(true);
    }
}
