package bot.jsonsave;

import bot.pg.Player;

import java.util.ArrayList;

public class PlayerListRegistred {

    private ArrayList<Player> list;

    public PlayerListRegistred() {
        list = new ArrayList<Player>();
    }

    public ArrayList<Player> getList() {
        return list;
    }

    public void setList(ArrayList<Player> list) {
        this.list = list;
    }
}
