package bot.jsonsave;

import bot.twitchbot.ConfigurationBot;
import com.google.gson.Gson;

import java.util.HashMap;

public class Salvataggio {
    private Gson gson;
    private PlayerListRegistred playerListRegistred;
    private ConfigurationBot configurationBot;
    private HashMap<String, Integer> generalSettings;

    public Salvataggio(PlayerListRegistred playerListRegistred) {
        this.playerListRegistred = playerListRegistred;
    }

    public Salvataggio(ConfigurationBot configurationBot) {
        this.configurationBot = configurationBot;
    }

    public Salvataggio(HashMap<String, Integer> generalSettings) {
        this.generalSettings = generalSettings;
    }

    public Salvataggio() {}

    public HashMap<String, Integer> getGeneralSettings() {
        return generalSettings;
    }

    public void setGeneralSettings(HashMap<String, Integer> generalSettings) {
        this.generalSettings = generalSettings;
    }

    public void doSave() {
        //TODO PRENDI LE INFO E LE SALVI NEL FILE JSON
    }

    public PlayerListRegistred getPlayerListRegistred() {
        return playerListRegistred;
    }

    public void setPlayerListRegistred(PlayerListRegistred playerListRegistred) {
        this.playerListRegistred = playerListRegistred;
    }

    public ConfigurationBot getConfigurationBot() {
        return configurationBot;
    }

    public void setConfigurationBot(ConfigurationBot configurationBot) {
        this.configurationBot = configurationBot;
    }
}
